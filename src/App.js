import avatar from "./assets/images/48.jpg";
import "./App.css";

function App() {
  return (
    <div className="devcamp-container">
      <img src={avatar} class="devcamp-avatar" />
      <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      <p class="devcamp-intro"><span class="devcamp-name">Tammy Stevens</span> . Front End Developer</p>
    </div>
  );
}

export default App;
